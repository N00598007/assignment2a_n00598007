﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content runat="server" ContentPlaceHolderID="title">
       <p>Digital Design</p> 
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="description">
       <p>Students will design and build a website mock-up using a graphic application tool such as Photoshop. 
          Based on their mock-up or an existing one, they will then develop the HTML or CSS code to create a 
          finished page layout. Using online resources, students will also implement and code the structure of
          a responsive website.</p>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="prof">
       <p>Professor: Joanna Kommala</p> 
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="code">
       <p>Course Code: HTTP 5104</p> 
</asp:Content>